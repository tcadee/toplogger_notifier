import requests
from datetime import datetime, timedelta
from time import sleep


def format_date_item(item):
    if item < 10:
        return f"0{item}"
    else:
        return item


def check_working_hours():
    now = datetime.utcnow() + timedelta(hours=2)
    if now.weekday() <= 3:
        if now.hour < 16:
            return False
    return True


start = datetime(2021, 5, 12, 18)
end = datetime(2021, 5, 12, 22)
already_send = set()
token = "1792408361:AAEEsbbYz4vxnsQ2GGd6MsRgsZD9gs0QF6s"
id = "1714288714"
message = "Starting toplogger service..."
requests.get(
    url=f"https://api.telegram.org/bot{token}/sendMessage?chat_id={id}&text={message}")
while True:
    now = datetime.utcnow() + timedelta(hours=2)
    if now.hour == 0:
        already_send = set()
    year = now.year
    month = format_date_item(now.month)
    day = format_date_item(now.day)
    response = requests.request(
        method='get', url=f"https://api.toplogger.nu/v1/gyms/11/slots?date={year}-{month}-{day}&reservation_area_id=67&slim=true").json()
    free_slots = [datetime.strptime(slot['start_at'], "%Y-%m-%dT%H:%M:%S.000+02:00") for slot in response if slot['spots'] - slot['spots_booked'] > 0
                  and not slot['require_password']]
    free_slots = [slot.strftime("%H:%M") for slot in free_slots if slot <= end
                  and slot >= start
                  and slot.strftime("%H:%M") not in already_send]
    # already_send.update(free_slots)
    if free_slots:
        for slot in free_slots:
            message = f'{slot} is free!'
            requests.get(
                url=f"https://api.telegram.org/bot{token}/sendMessage?chat_id={id}&text={message}")
    sleep(10)
